import React from 'react';
import PageContainer from "../PageContainer/PageContainer";
import Stopwatch from "../AucPage/Stopwatch/Stopwatch";

const StopwatchPage = () => {
    return (
        <PageContainer>
            <Stopwatch />
        </PageContainer>
    );
};

export default StopwatchPage;
